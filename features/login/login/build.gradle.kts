plugins {
    id("com.android.library")
    id("org.jetbrains.kotlin.android")
    id("dagger.hilt.android.plugin")
    kotlin("kapt")
}

android {
    namespace = "co.bjkarper.sample.login"
    compileSdk = AndroidVersions.COMPILE_SDK

    defaultConfig {
        minSdk = AndroidVersions.MINIMUM_SDK
        targetSdk = AndroidVersions.TARGET_SDK

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        consumerProguardFiles("consumer-rules.pro")
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
    kapt {
        correctErrorTypes = true
    }
    buildFeatures {
        compose = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = Compose.VERSION
    }
}

dependencies {

    //implementation(project(":profile-data"))

    implementation(Kotlin.COROUTINES)
    implementation(kotlin("reflect"))

    implementation(Compose.UI)
    implementation(Compose.MATERIAL)
    implementation(Compose.LIVEDATA)
    implementation(Compose.FOUNDATION)
    implementation(Compose.MATERIAL)
    implementation(Compose.MATERIAL_ICONS_EXTENDED)
    implementation(Compose.TOOLING)

    implementation(Coil.COIL)
    implementation(Coil.COIL_COMPOSE)

    implementation(Dagger.HILT)
    implementation(Dagger.HILT_COMPOSE_NAVIGATION)
    kapt(Dagger.HILT_COMPILER)

    testImplementation(JUnit.JUNIT)
    testImplementation(Kotlin.COROUTINES)
    testImplementation(JUnit.COROUTINES_JUNIT)
    testImplementation(MockK.MOCKK)

    androidTestImplementation(JUnit.COMPOSE_JUNIT)
    androidTestImplementation(JUnit.COMPOSE_TEST)
}