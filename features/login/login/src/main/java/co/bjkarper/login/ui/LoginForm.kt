package co.bjkarper.login.ui

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import co.bjkarper.login.LoginViewModel
import kotlinx.coroutines.delay

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun LoginForm() {
    val viewModel = hiltViewModel<LoginViewModel>()

    var username by rememberSaveable { mutableStateOf("") }
    var password by rememberSaveable { mutableStateOf("") }

    val isFormValid by viewModel.isLoginFormValid.collectAsState(initial = false)

    val keyboardController = LocalSoftwareKeyboardController.current
    val focusRequester = remember { FocusRequester() }

    Sample.Form.Field(
        label = "Username",
        value = username,
        error = "",
        keyboardOptions = KeyboardOptions(imeAction = ImeAction.Next),
        modifier = Modifier.focusRequester(focusRequester = focusRequester)
    ) {
        username = it
        viewModel.isUsernameValid(username)
    }

    Sample.Form.PasswordField(
        label = "Password",
        value = password,
        error = "",
        keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done),
        keyboardActions = KeyboardActions(onDone = { keyboardController?.hide() }),
        modifier = Modifier
            .padding(top = 16.dp)
    ) {
        password = it
        viewModel.isPasswordValid(password)
    }

    Button(
        modifier = Modifier
            .padding(top = 32.dp)
            .fillMaxWidth(0.6f),
        enabled = isFormValid,
        onClick = {
            viewModel.loginUser(username, password)
        }
    ) {
        Text("Login")
    }

    LaunchedEffect("") {
        delay(300)
        viewModel.isFormValid()
        focusRequester.requestFocus()
    }
}