package co.bjkarper.login.ui

import androidx.compose.runtime.Composable

class Sample {

    object Form {
        val Field: Field
        @Composable
        get() = Field()

        val PasswordField: PasswordField
        @Composable
        get() = PasswordField()
    }
}

class Field
class PasswordField