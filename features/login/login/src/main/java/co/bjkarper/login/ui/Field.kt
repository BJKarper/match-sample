package co.bjkarper.login.ui

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.defaultMinSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp

@Composable
fun Sample.Form.Field(
    modifier: Modifier = Modifier,
    label: String,
    value: String,
    error: String,
    keyboardOptions: KeyboardOptions = KeyboardOptions(),
    onChange: (String) -> Unit = {}
) {
    Column(
        modifier = modifier
            .fillMaxWidth()
            .wrapContentHeight()
    ) {
        TextField(
            value = value,
            onValueChange = onChange,
            label = { Text(label) },
            maxLines = 1,
            keyboardOptions = keyboardOptions,
            modifier = Modifier
                .defaultMinSize(minHeight = 48.dp, minWidth = 200.dp)
                .fillMaxWidth()
        )
        if (error.isNotBlank()) {
            Text(
                text = error,
                color = Color.Red,
            )
        }
    }
}

@Preview
@Composable
fun FieldPreview() {
    Sample.Form.Field(label = "Username", value = "Value", error = "Error")
}