package co.bjkarper.login

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor() : ViewModel() {

    private val _isLoginFormValid = MutableStateFlow(false)
    val isLoginFormValid = _isLoginFormValid.asStateFlow()

    private val _loginResponse = MutableStateFlow(false)
    val loginResponse = _loginResponse.asStateFlow()

    private var usernameValid = false
    private var passwordValid = false

    fun isUsernameValid(username: String) {
        usernameValid = username.isNotEmpty()
        isFormValid()
    }

    fun isPasswordValid(password: String) {
        passwordValid = password.isNotEmpty()
        isFormValid()
    }

    fun isFormValid() {
        viewModelScope.launch {
            _isLoginFormValid.emit((usernameValid && passwordValid))
        }
    }

    fun loginUser(username: String, password: String) {
        viewModelScope.launch {
            //TODO:: Replace with actual login api sometime and stop using mocked response
            //For implementation example see ProfileViewModel.kt
            //loginRepository.loginUser(username, password)
            _loginResponse.emit(true)
            delay(500)
            _loginResponse.emit(false)
        }
    }
}