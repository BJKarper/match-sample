package co.bjkarper.login.ui

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Visibility
import androidx.compose.material.icons.filled.VisibilityOff
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.setValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp

@Composable
fun Sample.Form.PasswordField(
    modifier: Modifier = Modifier,
    label: String,
    value: String,
    error: String,
    keyboardOptions: KeyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Password),
    keyboardActions: KeyboardActions = KeyboardActions(),
    onChange: (String) -> Unit = {}
) {

    var showPasswordField: Boolean by remember { mutableStateOf(false) }

    Column(
        modifier = modifier
            .fillMaxWidth()
            .wrapContentHeight()
    ) {
        Box(
            modifier = modifier
                .fillMaxWidth()
                .wrapContentHeight()
        ) {
            TextField(
                value = value,
                onValueChange = onChange,
                label = { Text(label) },
                maxLines = 1,
                visualTransformation = if (showPasswordField) {
                    VisualTransformation.None
                } else {
                    PasswordVisualTransformation()
                },
                keyboardOptions = keyboardOptions,
                keyboardActions = keyboardActions,
                modifier = Modifier
                    .defaultMinSize(minHeight = 48.dp, minWidth = 200.dp)
                    .fillMaxWidth()
                    .align(Alignment.CenterStart)
            )

            ToggleVisibilityIcon(
                showPasswordField = showPasswordField,
                modifier = Modifier.align(Alignment.CenterEnd)
            ) {
                showPasswordField = !showPasswordField
            }
        }
        if (error.isNotBlank()) {
            Text(
                text = error,
                color = Color.Red,
            )
        }
    }
}

@Composable
private fun ToggleVisibilityIcon(
    modifier: Modifier = Modifier,
    showPasswordField: Boolean,
    onClick: () -> Unit
) {
    val iconModifier = modifier
        .defaultMinSize(28.dp, 28.dp)
        .padding(20.dp)
        .clickable { onClick.invoke() }
    if (showPasswordField) {
        Icon(
            imageVector = Icons.Filled.Visibility,
            contentDescription = "Password Visibility Off",
            tint = MaterialTheme.colors.primaryVariant,
            modifier = iconModifier
        )
    } else {
        Icon(
            imageVector = Icons.Filled.VisibilityOff,
            contentDescription = "Password Visibility On",
            tint = MaterialTheme.colors.primaryVariant,
            modifier = iconModifier
        )
    }
}

@Preview
@Composable
fun PasswordFieldPreview() {
    Sample.Form.Field(label = "Password", value = "", error = "")
}