package co.bjkarper.login.ui

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.navigation.NavController
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController

@Composable
fun DefaultTopBar(navController: NavController = rememberNavController()) {

    val currentBackStackEntry by navController.currentBackStackEntryAsState()

    val label = currentBackStackEntry?.destination?.route ?: ""

    TopAppBar(
        modifier = Modifier
            .fillMaxWidth()
            .wrapContentHeight(),
        title = { GetTitleText(title = label) },
        backgroundColor = MaterialTheme.colors.primary
    )
}

@Composable
private fun GetTitleText(title: String) {
    return Text(
        text = title,
        color = Color.White,
        style = MaterialTheme.typography.h6
    )
}

@Composable
private fun GetNavigationIcon(navController: NavController) {
    IconButton(
        modifier = Modifier.wrapContentSize(),
        onClick = { navController.navigateUp() }
    ) {
        Icon(
            modifier = Modifier.wrapContentSize(),
            imageVector = Icons.Filled.ArrowBack,
            contentDescription = "Back"
        )
    }
}