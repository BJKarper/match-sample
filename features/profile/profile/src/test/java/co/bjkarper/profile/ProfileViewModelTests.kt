package co.bjkarper.profile

import co.bjkarper.profile_data.obj.ConfigResponse
import co.bjkarper.profile_data.obj.Profile
import co.bjkarper.profile_data.obj.UsersResponse
import co.bjkarper.profile_data.repo.IProfileRepository
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.runTest
import org.junit.*
import org.junit.runner.RunWith
import org.junit.runners.JUnit4


@RunWith(JUnit4::class)
@ExperimentalCoroutinesApi
class ProfileViewModelTests {

    @ExperimentalCoroutinesApi
    @get:Rule
    val mainCoroutineRule = MainCoroutineRule()

    private val mockRepository = mockk<IProfileRepository>()

    private val viewModel = ProfileViewModel(
        profileRepository = mockRepository
    )

    @Before
    fun setUp() {

    }

    @After
    fun tearDown() {

    }

    @Test
    fun `Test Config Returns Valid`() = runTest {
        val retConfig = ConfigResponse(listOf("name", "photo", "gender", "about"))
        coEvery { mockRepository.getConfig() } returns flow { emit(retConfig) }

        mainCoroutineRule.launch {

            viewModel.config.collect {
                Assert.assertTrue(it.size == 4)
            }
        }
    }

    @Test
    fun `Test Users Return Valid`() = runTest {
        val retProfile = UsersResponse(listOf(Profile(name = "Name1"), Profile(name = "Name2")))
        coEvery { mockRepository.getProfiles() } returns flow { emit(retProfile) }

        mainCoroutineRule.launch {

            viewModel.loadNextUser()

            viewModel.currentUser.collect {
                Assert.assertTrue((it.name == "Name1"))
            }
        }
    }

    @Test
    fun `Test Initial Loading Complete`() = runTest {

        mainCoroutineRule.launch {

            viewModel.initialLoading()

            val isLoading = viewModel.isLoading.value

            Assert.assertTrue(!isLoading)
        }
    }

    @Test
    fun `Test Load Next Profile`() = runTest {

        mainCoroutineRule.launch {

            viewModel.loadNextUser()
            viewModel.loadNextUser()

            viewModel.currentUser.collect {
                Assert.assertTrue(it.name == "Name2")
            }
        }
    }

    @Test
    fun `Test Load Previous Profile`() = runTest {

        mainCoroutineRule.launch {

            viewModel.loadNextUser()
            viewModel.loadNextUser()
            viewModel.loadPreviousUser()
            viewModel.loadPreviousUser()

            viewModel.currentUser.collect {
                Assert.assertTrue(it.name == "Name1")
            }
        }
    }

    @Test
    fun `Test Fullscreen Image Toggle`() = runTest {

        mainCoroutineRule.launch {

            viewModel.toggleFullscreenImage(true)
            val isFullscreen = viewModel.isFullscreenImage.value
            viewModel.toggleFullscreenImage(false)
            val isNotFullscreen = viewModel.isFullscreenImage.value

            Assert.assertTrue(isFullscreen)
            Assert.assertFalse(isNotFullscreen)

        }
    }
}