package co.bjkarper.profile.ui

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Icon
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import co.bjkarper.profile_data.obj.IProfile
import coil.compose.AsyncImage
import coil.request.ImageRequest

@Composable
fun ProfileFullscreenPicture(
    modifier: Modifier = Modifier,
    user: IProfile,
    onClose: () -> Unit = {}
) {

    Box(
        modifier = modifier
            .fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        AsyncImage(
            model = ImageRequest.Builder(LocalContext.current)
                .data(user.photo)
                .build(),
            contentDescription = "${user.name} Profile Picture",
            contentScale = ContentScale.FillWidth,
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentHeight()
        )
        Box(
            modifier = Modifier
                .size(48.dp)
                .padding(8.dp)
                .align(Alignment.TopEnd)
                .clip(CircleShape)
                .background(Color.LightGray.copy(0.66f))
        ) {
            Icon(
                imageVector = Icons.Default.Close,
                contentDescription = "Close",
                tint = Color.White,
                modifier = Modifier
                    .size(40.dp)
                    .padding(2.dp)
                    .align(Alignment.Center)
                    .clickable { onClose.invoke() }
            )
        }
    }
}