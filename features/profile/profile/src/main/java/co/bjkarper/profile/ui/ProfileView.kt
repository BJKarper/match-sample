package co.bjkarper.profile.ui

import androidx.compose.foundation.layout.Column
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import co.bjkarper.profile_data.obj.ConfigType
import co.bjkarper.profile_data.obj.IProfile
import co.bjkarper.profile_data.obj.Profile

@Composable
fun ProfileView(config: List<String>, user: IProfile, onClick: () -> Unit = {}) {
    Column {
        config.forEach { property ->
            when (ConfigType.valueOf(property.uppercase())) {
                ConfigType.NAME -> {
                    if (user.name.isNotEmpty()) {
                        ProfileNameSection(name = user.name)
                    }
                }
                ConfigType.PHOTO -> {
                    if (user.photo.isNotEmpty()) {
                        ProfileImageSection(
                            name = user.name,
                            imageUrl = user.photo,
                            onClick = { onClick.invoke() })
                    }
                }
                ConfigType.GENDER -> {
                    if (user.gender.isNotEmpty()) {
                        ProfileSection(header = property, content = user.gender)
                    }
                }
                ConfigType.ABOUT -> {
                    if (user.about.isNotEmpty()) {
                        ProfileSection(header = property, content = user.about)
                    }
                }
                ConfigType.SCHOOL -> {
                    if (user.school.isNotEmpty()) {
                        ProfileSection(header = property, content = user.school)
                    }
                }
                ConfigType.HOBBIES -> {
                    if (user.hobbies.isNotEmpty()) {
                        ProfileSection(header = property, content = user.getHobbiesString())
                    }
                }
            }
        }
    }
}

@Preview
@Composable
fun PreviewProfileView() {
    ProfileView(config = listOf("Name", "Gender", "About"), user = Profile(name = "Name", gender = "m", about = "About text"))
}