package co.bjkarper.profile.ui

import androidx.compose.foundation.layout.*
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import co.bjkarper.profile.ProfileViewModel

@Composable
fun ProfileLayout(
    modifier: Modifier = Modifier
) {
    val profileViewModel = hiltViewModel<ProfileViewModel>()

    val config by profileViewModel.config.collectAsState(initial = emptyList())
    val user by profileViewModel.currentUser.collectAsState(initial = null)
    val isLoading by profileViewModel.isLoading.collectAsState(initial = true)

    val toggleFullscreenImage by profileViewModel.isFullscreenImage.collectAsState(initial = false)
    val toggleNextButton by profileViewModel.toggleNextButton.collectAsState(initial = true)
    val togglePreviousButton by profileViewModel.togglePreviousButton.collectAsState(initial = false)

    profileViewModel.initialLoading()

    Box(modifier = modifier) {
        if (!isLoading && user != null) {
            if (toggleFullscreenImage) {
                ProfileFullscreenPicture(
                    user = user!!,
                    modifier = Modifier.align(Alignment.Center)
                ) {
                    profileViewModel.toggleFullscreenImage(false)
                }
            } else {
                ProfileView(config = config, user = user!!) {
                    profileViewModel.toggleFullscreenImage(true)
                }
                ProfileBottomBar(
                    onNext = { profileViewModel.loadNextUser() },
                    onPrev = { profileViewModel.loadPreviousUser() },
                    showNext = toggleNextButton,
                    showPrev = togglePreviousButton,
                    modifier = Modifier
                        .fillMaxWidth()
                        .wrapContentHeight()
                        .padding(bottom = 32.dp)
                        .align(Alignment.BottomCenter)
                )
            }
        } else {
            CircularProgressIndicator(
                modifier = Modifier
                    .size(64.dp)
                    .align(Alignment.Center)
            )
        }
    }
}
