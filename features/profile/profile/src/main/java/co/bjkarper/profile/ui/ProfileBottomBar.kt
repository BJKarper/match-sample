package co.bjkarper.profile.ui

import androidx.compose.foundation.layout.*
import androidx.compose.material.FloatingActionButton
import androidx.compose.material.Icon
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.ArrowForward
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp

@Composable
fun ProfileBottomBar(
    onNext: () -> Unit = {},
    onPrev: () -> Unit = {},
    showNext: Boolean = true,
    showPrev: Boolean = true,
    modifier: Modifier = Modifier
) {
    Box(modifier = modifier) {
        Row(
            modifier = Modifier
                .wrapContentWidth()
                .wrapContentHeight()
                .align(Alignment.BottomCenter)
        ) {
            if (showPrev) {
                FloatingActionButton(
                    onClick = onPrev,
                    modifier = Modifier
                        .padding(horizontal = 8.dp)
                        .testTag("Previous"),
                ) {
                    Icon(Icons.Default.ArrowBack, contentDescription = "Previous")
                }
            }

            if (showNext) {
                FloatingActionButton(
                    onClick = onNext,
                    modifier = Modifier
                        .padding(horizontal = 8.dp)
                        .testTag("Next")
                ) {
                    Icon(Icons.Default.ArrowForward, contentDescription = "Next")
                }
            }
        }
    }
}

@Preview
@Composable
fun PreviewProfileBottomBar() {
    ProfileBottomBar(showNext = true, showPrev = true)
}