package co.bjkarper.profile

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import co.bjkarper.profile_data.obj.IProfile
import co.bjkarper.profile_data.obj.Profile
import co.bjkarper.profile_data.repo.IProfileRepository

import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

@HiltViewModel
class ProfileViewModel @Inject constructor(
    private val profileRepository: IProfileRepository
) : ViewModel() {

    private var _users = emptyList<Profile>()
    private var _position = 0

    private val _isLoading = MutableStateFlow(true)
    val isLoading = _isLoading.asStateFlow()

    private val _isFullscreenImage = MutableStateFlow(false)
    val isFullscreenImage = _isFullscreenImage.asStateFlow()

    private val _config = MutableSharedFlow<List<String>>()
    val config = _config.asSharedFlow()

    private val _currentUser = MutableSharedFlow<IProfile>()
    val currentUser = _currentUser.asSharedFlow()

    private val _toggleNextButton = MutableSharedFlow<Boolean>()
    val toggleNextButton = _toggleNextButton.asSharedFlow()

    private val _togglePreviousButton = MutableSharedFlow<Boolean>()
    val togglePreviousButton = _togglePreviousButton.asSharedFlow()

    init {
        getProfileConfig()
        getUsers()
    }

    fun initialLoading() {
        viewModelScope.launch {
            _config.zip(profileRepository.getProfiles()) { _, _ -> }.collect {
                _isLoading.emit(false)
                loadUserByPosition()
                toggleButtons()
            }
        }
    }

    fun loadNextUser() {
        _position++
        if (_position != _users.size) {
            toggleButtons()
            loadUserByPosition()
        }
    }

    fun loadPreviousUser() {
        _position--
        if (_position != -1) {
            toggleButtons()
            loadUserByPosition()
        }
    }

    fun toggleFullscreenImage(isClicked: Boolean) {
        viewModelScope.launch {
            _isFullscreenImage.emit(isClicked)
        }
    }

    fun getUserById(userId:Int): IProfile {
        return _users.first {
            it.id == userId
        }
    }

    private fun getProfileConfig() {
        viewModelScope.launch(Dispatchers.IO) {
            val correctedList = mutableListOf<String>()
            profileRepository.getConfig().collect { response ->
                response.profile.forEach { config ->
                    correctedList.add(config.replaceFirstChar {
                        if (it.isLowerCase()) it.titlecase(
                            Locale.US
                        ) else it.toString()
                    })
                }
                _config.emit(correctedList)
            }
        }
    }

    private fun getUsers() {
        viewModelScope.launch(Dispatchers.IO) {
            profileRepository.getProfiles().collect {
                _users = it.users
            }
        }
    }

    private fun toggleButtons() {
        viewModelScope.launch {
            when (_position) {
                0 -> {
                    _toggleNextButton.emit(true)
                    _togglePreviousButton.emit(false)
                }
                (_users.size - 1) -> {
                    _toggleNextButton.emit(false)
                    _togglePreviousButton.emit(true)
                }
                else -> {
                    _toggleNextButton.emit(true)
                    _togglePreviousButton.emit(true)
                }
            }
        }
    }

    private fun loadUserByPosition() {
        if (_users.isNotEmpty()) {
            viewModelScope.launch {
                val user = _users[_position]
                _currentUser.emit(user)
            }
        }
    }

}