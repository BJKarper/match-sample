package co.bjkarper.profile

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithTag
import co.bjkarper.profile.ui.ProfileBottomBar
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class ProfileBottomBarTests {

    @get:Rule
    val composeTestRule = createComposeRule()

    @Test
    fun testBottomBarOnlyDisplaysNextButton() {
        composeTestRule.setContent {
            ProfileBottomBar(showNext = true)
        }

        composeTestRule.onNodeWithTag("Next").assertIsDisplayed()
    }

    @Test
    fun testBottomBarOnlyDisplaysPreviousButton() {
        composeTestRule.setContent {
            ProfileBottomBar(showPrev = true)
        }

        composeTestRule.onNodeWithTag("Previous").assertIsDisplayed()
    }

    @Test
    fun testBottomBarOnlyDisplaysBothButtons() {
        composeTestRule.setContent {
            ProfileBottomBar(showPrev = true, showNext = true)
        }

        composeTestRule.onNodeWithTag("Next").assertIsDisplayed()
        composeTestRule.onNodeWithTag("Previous").assertIsDisplayed()
    }
}