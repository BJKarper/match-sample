package co.bjkarper.profile_data.di


import co.bjkarper.profile_data.api.ProfileAPI
import co.bjkarper.profile_data.repo.IProfileRepository
import co.bjkarper.profile_data.repo.ProfileRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped
import retrofit2.Retrofit

@Module
@InstallIn(ViewModelComponent::class)
class ProfileDataModule {

    @Provides
    @ViewModelScoped
    fun providesProfileRepository(api: ProfileAPI): IProfileRepository {
        return ProfileRepository(api)
    }

    @Provides
    @ViewModelScoped
    fun providesProfileAPI(retrofit: Retrofit): ProfileAPI {
        return retrofit.create(ProfileAPI::class.java)
    }

}