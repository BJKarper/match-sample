package co.bjkarper.profile_data.obj

data class UsersResponse(
    var users: List<Profile> = emptyList()
)