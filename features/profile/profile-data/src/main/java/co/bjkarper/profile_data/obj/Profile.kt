package co.bjkarper.profile_data.obj

import java.util.*

interface IProfile {
    val id: Int
    var name: String
    var gender: String
    var about: String
    var photo: String
    var school: String
    var hobbies: List<String>

    fun getHobbiesString(): String
}

class Profile(
    id: Int = 0,
    name: String = "",
    gender: String = "",
    about: String = "",
    photo: String = "",
    school: String = "",
    hobbies: List<String> = emptyList()
) : IProfile {

    override val id: Int = 0
    override var name: String = ""
        get() = capitalizeString(field)
    override var gender: String = ""
        get() = if (field.lowercase() == "m") "Male" else "Female"
    override var about: String = ""
        get() = capitalizeString(field)
    override var photo: String = ""
    override var school: String = ""
        get() = capitalizeString(field)
    override var hobbies: List<String> = emptyList()

    override fun getHobbiesString(): String {
        return this.hobbies.joinToString(separator = ", ") { it }
    }
}

private fun capitalizeString(string: String): String {
    return string.replaceFirstChar {
        if (it.isLowerCase()) it.titlecase(Locale.US)
        else it.toString()
    }
}