package co.bjkarper.profile_data.obj

data class ConfigResponse(
    var profile:List<String> = emptyList()
)