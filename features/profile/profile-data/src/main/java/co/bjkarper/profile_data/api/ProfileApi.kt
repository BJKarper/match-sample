package co.bjkarper.profile_data.api

import co.bjkarper.profile_data.obj.ConfigResponse
import co.bjkarper.profile_data.obj.UsersResponse
import retrofit2.http.GET

interface ProfileAPI {

    @GET("/users")
    suspend fun getUsers(): UsersResponse

    @GET("/config")
    suspend fun getConfig(): ConfigResponse
}