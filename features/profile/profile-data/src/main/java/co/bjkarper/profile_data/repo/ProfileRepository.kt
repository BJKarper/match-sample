package co.bjkarper.profile_data.repo


import co.bjkarper.profile_data.api.ProfileAPI
import co.bjkarper.profile_data.obj.ConfigResponse
import co.bjkarper.profile_data.obj.UsersResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

interface IProfileRepository {

    fun getProfiles(): Flow<UsersResponse>

    fun getConfig(): Flow<ConfigResponse>
}

class ProfileRepository @Inject constructor(
    private val profileAPI: ProfileAPI
): IProfileRepository {

    override fun getProfiles(): Flow<UsersResponse> {
        return flow {
            emit(profileAPI.getUsers())
        }.flowOn(Dispatchers.IO)
    }

    override fun getConfig(): Flow<ConfigResponse> {
        return flow {
            emit(profileAPI.getConfig())
        }.flowOn(Dispatchers.IO)
    }
}