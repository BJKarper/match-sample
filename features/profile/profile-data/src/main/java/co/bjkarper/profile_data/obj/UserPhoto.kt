package co.bjkarper.profile_data.obj

data class UserPhoto(
    val imageUrl: String,
    val contentDescription: String
)
