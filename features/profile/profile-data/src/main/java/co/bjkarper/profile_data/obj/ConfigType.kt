package co.bjkarper.profile_data.obj

enum class ConfigType {
    NAME, PHOTO, GENDER, ABOUT, SCHOOL, HOBBIES
}