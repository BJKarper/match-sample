package co.bjkarper.profile_data

import co.bjkarper.profile_data.api.ProfileAPI
import co.bjkarper.profile_data.obj.ConfigResponse
import co.bjkarper.profile_data.obj.Profile
import co.bjkarper.profile_data.obj.UsersResponse
import co.bjkarper.profile_data.repo.ProfileRepository
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4


@RunWith(JUnit4::class)
@ExperimentalCoroutinesApi
class ProfileRepositoryTests {

    private val mockApi = mockk<ProfileAPI>()

    private val profileRepository = ProfileRepository(mockApi)

    @Test
    fun `Test Config Returns Valid`() = runTest {
        var response = ConfigResponse()
        coEvery { mockApi.getConfig() } returns ConfigResponse(
            listOf(
                "name",
                "photo",
                "gender",
                "about"
            )
        )

        profileRepository.getConfig().collect {
            response = it
        }

        Assert.assertTrue(response.profile.size == 4)
    }

    @Test
    fun `Test Users Return Valid`() = runTest {
        var response = UsersResponse()
        coEvery { mockApi.getUsers() } returns UsersResponse(listOf(Profile()))

        profileRepository.getProfiles().collect {
            response = it
        }

        Assert.assertTrue((response.users.size == 1))
    }
}