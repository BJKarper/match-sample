plugins {
    id("com.android.application")
    id("kotlin-android")
    id("dagger.hilt.android.plugin")
    kotlin("kapt")
}

android {
    namespace = "co.bjkarper.sample"

    buildToolsVersion = AndroidVersions.BUILD_TOOLS
    compileSdk = AndroidVersions.COMPILE_SDK

    defaultConfig {
        applicationId = "co.bjkarper.sample"
        minSdk = AndroidVersions.MINIMUM_SDK
        targetSdk = AndroidVersions.TARGET_SDK
        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
    kapt {
        correctErrorTypes = true
    }
    buildFeatures {
        compose = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = Compose.VERSION
    }
}

dependencies {

    implementation(project(":login"))
    implementation(project(":profile"))

    implementation(AndroidX.CORE)

    implementation(ComposeActivity.ACTIVITY)

    implementation(Compose.UI)
    implementation(Compose.LIFECYCLE_VIEWMODEL)
    implementation(Compose.MATERIAL)

    implementation(ComposeNavigation.NAVIGATION)

    implementation(Dagger.HILT)
    kapt(Dagger.HILT_COMPILER)

}