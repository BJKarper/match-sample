package co.bjkarper.sample

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import co.bjkarper.login.ui.DefaultTopBar
import co.bjkarper.login.ui.LoginLayout
import co.bjkarper.profile.ui.ProfileLayout
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            val navController = rememberNavController()

            MaterialTheme {
                Scaffold(
                    topBar = {
                        DefaultTopBar(navController = navController)
                    }
                ) {
                    NavHost(
                        navController = navController,
                        startDestination = "Login"
                    ) {
                        composable("Login") { LoginLayout(navController) }
                        composable("Profiles") {
                            ProfileLayout(
                                modifier = Modifier
                                    .fillMaxSize()
                                    .verticalScroll(rememberScrollState())
                                    .padding(bottom = 12.dp)
                            )
                        }
                    }
                }
            }
        }
    }
}