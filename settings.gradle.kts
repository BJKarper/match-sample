pluginManagement {
    repositories {
        gradlePluginPortal()
        google()
        mavenCentral()
    }
}
dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        mavenCentral()
    }
}
rootProject.name = "Sample"
include(":app")

// Core Modules
include(":network-core")
project(":network-core").projectDir = File("core/network-core")

// Features
//////  Login
include(":login")
project(":login").projectDir = File("features/login/login")

//////  Profile
include(":profile")
project(":profile").projectDir = File("features/profile/profile")
include(":profile-data")
project(":profile-data").projectDir = File("features/profile/profile-data")

