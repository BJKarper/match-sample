
object Kotlin {
    private const val VERSION = "1.5.31"
    const val STD_LIB = "org.jetbrains.kotlin:kotlin-stdlib:$VERSION"

    private const val COROUTINES_VERSION = "1.6.0"
    const val COROUTINES = "org.jetbrains.kotlinx:kotlinx-coroutines-android:${COROUTINES_VERSION}"
}

object AndroidX {
    private const val CORE_VERSION = "1.7.0"
    private const val VERSION = "1.4.1"
    const val CORE = "androidx.core:core-ktx:$CORE_VERSION"
    const val APPCOMPAT = "androidx.appcompat:appcompat:$VERSION"
    const val FRAGMENT = "androidx.fragment:fragment-ktx:$VERSION"
}

object Compose {
    const val VERSION = "1.1.1"
    const val UI = "androidx.compose.ui:ui:$VERSION"
    const val TOOLING = "androidx.compose.ui:ui-tooling:$VERSION"
    const val FOUNDATION = "androidx.compose.foundation:foundation:$VERSION"
    const val MATERIAL = "androidx.compose.material:material:$VERSION"
    const val MATERIAL_ICONS_EXTENDED = "androidx.compose.material:material-icons-extended:$VERSION"
    const val LIFECYCLE_VIEWMODEL = "androidx.lifecycle:lifecycle-viewmodel-compose:2.5.0"
    const val LIVEDATA = "androidx.compose.runtime:runtime-livedata:$VERSION"
}

object ComposeActivity {
    private const val VERSION = "1.3.1"
    const val ACTIVITY = "androidx.activity:activity-compose:$VERSION"
}

object ComposeNavigation {
    private const val VERSION = "2.5.1"
    const val NAVIGATION = "androidx.navigation:navigation-compose:$VERSION"
}

object Coil {
    private const val VERSION = "2.1.0"
    const val COIL = "io.coil-kt:coil:$VERSION"
    const val COIL_COMPOSE = "io.coil-kt:coil-compose:$VERSION"
}

object Dagger {
    private const val VERSION = "2.42"
    const val HILT = "com.google.dagger:hilt-android:$VERSION"
    const val HILT_COMPOSE_NAVIGATION = "androidx.hilt:hilt-navigation-compose:1.0.0"
    const val HILT_COMPILER = "com.google.dagger:hilt-compiler:$VERSION"
}

object Retrofit {
    private const val VERSION = "2.9.0"
    const val RETROFIT = "com.squareup.retrofit2:retrofit:$VERSION"
    const val JACKSON = "com.squareup.retrofit2:converter-jackson:$VERSION"
}

object OkHttp {
    private const val VERSION = "4.10.0"
    const val OKHTTP = "com.squareup.okhttp3:okhttp:$VERSION"
    const val LOGGING_INTERCEPTOR = "com.squareup.okhttp3:logging-interceptor:$VERSION"
    const val MOCK_SERVER = "com.squareup.okhttp3:mockwebserver:$VERSION"
}

object MockK {
    private const val VERSION = "1.12.4"
    const val MOCKK = "io.mockk:mockk:$VERSION"
}

object JUnit {
    private const val JUNIT_VERSION = "4.13.2"
    const val JUNIT = "junit:junit:${JUNIT_VERSION}"

    private const val COMPOSE_VERSION = "1.1.1"
    const val COMPOSE_JUNIT = "androidx.compose.ui:ui-test-junit4:${COMPOSE_VERSION}"
    const val COMPOSE_TEST = "androidx.compose.ui:ui-test:${COMPOSE_VERSION}"

    private const val COROUTINES_VERSION = "1.6.0"
    const val COROUTINES_JUNIT = "org.jetbrains.kotlinx:kotlinx-coroutines-test:${COROUTINES_VERSION}"
}

object AndroidTest {
    private const val VERSION = "3.4.0"
    const val ESPRESSO_CORE = "androidx.test.espresso:espresso-core:$VERSION"

    private const val COMPOSE_VERSION = "1.0.0-beta08"
    const val COMPOSE_TESTING = "androidx.compose.ui:ui-test-junit4${COMPOSE_VERSION}"
}

object AndroidVersions {
    const val MINIMUM_SDK = 28
    const val TARGET_SDK = 32
    const val COMPILE_SDK = 32
    const val APP_COMPILE_SDK = "android-$COMPILE_SDK"
    const val BUILD_TOOLS = "32.0.0"
}