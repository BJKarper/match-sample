plugins {
    id("com.android.library")
    id("org.jetbrains.kotlin.android")
    id("dagger.hilt.android.plugin")
    kotlin("kapt")
}

android {
    namespace = "co.bjkarper.sample.network_core"
    compileSdk = AndroidVersions.COMPILE_SDK

    defaultConfig {
        minSdk = AndroidVersions.MINIMUM_SDK
        targetSdk = AndroidVersions.TARGET_SDK

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        consumerProguardFiles("consumer-rules.pro")
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
    kapt {
        correctErrorTypes = true
    }
}

dependencies {

    implementation(Dagger.HILT)
    kapt(Dagger.HILT_COMPILER)

    implementation(OkHttp.LOGGING_INTERCEPTOR)
    implementation(OkHttp.MOCK_SERVER)

    implementation(Retrofit.RETROFIT)
    implementation(Retrofit.JACKSON)

    testImplementation(JUnit.JUNIT)

    testImplementation(JUnit.JUNIT)
    testImplementation(JUnit.COROUTINES_JUNIT)
}