Sample App
== 

Sample App is a completely api driven app for experimenting with information displayed in a users profile.

Architecture
--

This application is designed using MVVM architecture and is laid our in a multimodule file structure as follows:

- app
- buildSrc -- Dependency Management
- core
    - network-core -- Provides Dagger Modules for Retrofit
- features
    - profile
        - profile -- UI module for Profile Section
        - profile-data -- module for datalayer, repository, api, other data sources

As the app is grown in features they will be given their own new folder section under features i.e. (features >> feature1 >> feature1/feature1-data)

When the app grows to a point where reusable UI components can be created or a unified app design is settled on.

A new section under core can be created. (core >> compose-core)

Libraries
--
The app utilizes the following libraries/languages:

Development Libraries
- Kotlin
- Kotlin-Coroutines
- AndroidX
- Compose
- Compose Navigation
- Coil
- Dagger Hilt
- Retrofit
- OkHttp

Testing Libraries
- Mockk
- JUnit Test
    - Compose Test
    - Coroutines Test
